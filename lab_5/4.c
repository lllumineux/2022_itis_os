#include <stdio.h>
#include <sys/stat.h>

struct stat st;

int main(int argc, char *argv[], char *envp[]) {
    FILE *file_1 = fopen(argv[1], "r");
    if (!file_1) {
        printf("Error while opening the first file");
        return 1;
    }

    FILE *file_2 = fopen(argv[2], "w+");
    if (!file_2) {
        printf("Error while opening the second file");
        return 1;
    }
    
    char c;

    while ((c = getc(file_1)) != EOF){
        fputc(c, file_2);
    }

    int closed_1 = fclose(file_1);
    if (closed_1) {
        printf("Error while closing the first file");
        return 1;
    }

    int closed_2 = fclose(file_2);
    if (closed_2) {
        printf("Error while closing the second file");
        return 1;
    }

    stat(argv[1], &st);
    chmod(argv[2], st.st_mode);

    return 0;
}
