#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <limits.h>
#include <libgen.h>
#include <time.h>

typedef struct Array {
    char **array;
    size_t size;
    size_t last_index;
} Array;

void array_init(Array *a) {
    a->array = malloc(100 * sizeof(char *));
    a->size = 100;
    a->last_index = 0;
}

void array_append(Array *a, char *element) {
    if (a->last_index == a->size) {
        a->size += 100;
        a->array = realloc(a->array, a->size * sizeof(char *));
    }
    a->array[a->last_index] = malloc(sizeof(element));
    strcpy(a->array[a->last_index++], element);
}

char* num_to_str(long long num, int base){
    if(num == 0) return "0";

    int sign = (num < 0);
    if(sign) num = -num;

    int i;
    static char res[64] = {0};

    for(i = 62; num && i; --i, num /= base) {
        res[i] = "0123456789abcdef"[num % base];
    }

    if(sign) res[i--] = '-';
    return &res[i+1];
}

int get_directory_content(Array *files_array, char* path, char* type) {
    DIR *dir = opendir(path);
    if (!dir) {
        printf("Error while opening \"%s\" directory", path);
        return 1;
    }

    struct dirent *d;

    while ((d = readdir(dir))) {
        char new_path[PATH_MAX];
        strcpy(new_path, path);
        strcat(new_path, "/");
        strcat(new_path, d->d_name);

        struct stat st;
        stat(new_path, &st);

        if (!strcmp(type, "files")) {
            if (S_ISREG(st.st_mode)) {
                array_append(files_array, new_path);
            }
        } else if (!strcmp(type, "dirs")) {
            if (S_ISDIR(st.st_mode) && strcmp(d->d_name, ".") && strcmp(d->d_name, "..")) {
                get_directory_content(files_array, new_path, "files");
                get_directory_content(files_array, new_path, "dirs");
            }
        }
    }

    int closed = closedir(dir);
    if (closed) {
        printf("Error while closing \"%s\" directory", path);
        return 1;
    }

    return 0;
}

bool compare_files(char* path_1, char* path_2) {
    FILE *file_1 = fopen(path_1, "r");
    if (!file_1) {
        printf("Error while opening \"%s\" file", path_1);
        return 1;
    }

    FILE *file_2 = fopen(path_2, "r");
    if (!file_2) {
        printf("Error while opening \"%s\" file", path_2);
        return 1;
    }

    bool same = true;
    char file_1_c;
    char file_2_c;

    while (1){
        file_1_c = getc(file_1);
        file_2_c = getc(file_2);

        if (file_1_c != file_2_c) {
            same = false;
            break;
        }

        if (file_1_c == EOF || file_2_c == EOF) {
            break;
        }
    }

    int closed_1 = fclose(file_1);
    if (closed_1) {
        printf("Error while closing \"%s\" file", path_1);
        return 1;
    }

    int closed_2 = fclose(file_2);
    if (closed_2) {
        printf("Error while closing \"%s\" file", path_2);
        return 1;
    }

    return same;
}

int main(int argc, char *argv[], char *envp[]) {
    Array dir1_files;
    array_init(&dir1_files);

    Array dir2_files;
    array_init(&dir2_files);

    get_directory_content(&dir1_files, argv[1], "dirs");
    get_directory_content(  &dir2_files, argv[2], "dirs");

    FILE *file = fopen(argv[3], "w+");
    if (!file) {
        printf("Error while opening \"%s\" file", argv[3]);
        return 1;
    }

    for (int i = 0; i < dir1_files.last_index; i++) {
        for (int c = 0; c < dir2_files.last_index; c++) {
            char* file_1 = dir1_files.array[i];
            char* file_2 = dir2_files.array[c];
            if (compare_files(file_1, file_2)) {
                struct stat st1;
                stat(file_1, &st1);
                char file_1_info[256] = "";
                strcat(file_1_info, "Name: ");
                strcat(file_1_info, basename(file_1));
                strcat(file_1_info, "\nSize (in bytes): ");
                strcat(file_1_info, num_to_str(st1.st_size, 10));
                strcat(file_1_info, "\nCreation Time: ");
                char file_1_creation_time[128] = "";
                strftime(file_1_creation_time, 128, "%d.%m.%Y %H:%M", gmtime(&st1.st_birthtimespec.tv_sec));
                strcat(file_1_info, file_1_creation_time);
                strcat(file_1_info, "\nAccess Rights: ");
                char file_1_access_rights[16] = "";
                strmode(st1.st_mode, file_1_access_rights);
                strcat(file_1_info, file_1_access_rights);
                strcat(file_1_info, "\nIndex Descriptor Number: ");
                strcat(file_1_info, num_to_str(st1.st_ino, 10));

                struct stat st2;
                stat(file_2, &st2);
                char file_2_info[256] = "";
                strcat(file_2_info, "Name: ");
                strcat(file_2_info, basename(file_2));
                strcat(file_2_info, "\nSize (in bytes): ");
                strcat(file_2_info, num_to_str(st2.st_size, 10));
                strcat(file_2_info, "\nCreation Time: ");
                char file_2_creation_time[128] = "";
                strftime(file_2_creation_time, 128, "%d.%m.%Y %H:%M", gmtime(&st2.st_birthtimespec.tv_sec));
                strcat(file_2_info, file_2_creation_time);
                strcat(file_2_info, "\nAccess Rights: ");
                char file_2_access_rights[16] = "";
                strmode(st2.st_mode, file_2_access_rights);
                strcat(file_2_info, file_2_access_rights);
                strcat(file_2_info, "\nIndex Descriptor Number: ");
                strcat(file_2_info, num_to_str(st2.st_ino, 10));

                char result[1024] = "";
                strcat(result, file_1_info);
                strcat(result, "\n+\n");
                strcat(result, file_2_info);
                strcat(result, "\n\n\n");

                printf("%s", result);

                int counter = 0;
                for (int o = 0; ; o++) {
                    char sym = result[o];
                    putc(sym, file);
                    if (sym == '\n') counter++;
                    if (counter == 4 * 2 + 5) break;
                }
            }
        }
    }

    int closed = fclose(file);
    if (closed) {
        printf("Error while closing \"%s\" file", argv[3]);
        return 1;
    }

    return 0;
}
