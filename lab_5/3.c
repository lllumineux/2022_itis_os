#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *envp[]) {
    FILE* file = fopen(argv[1], "r");
    if (!file) {
        printf("Error while opening the file");
        return 1;
    }

    int rows = 0;
    if (argv[2]) {
        rows = atoi(argv[2]);
    }

    char user_c = '\n';
    bool stop = false;

    while (1) {
        if (user_c == '\n') {
            int counter = 0;

            while (rows == 0 || counter < rows) {
                char file_c = fgetc(file);

                if (file_c == EOF) {
                    stop = true;
                    break;
                } else if (file_c == '\n') {
                    counter++;
                }

                printf("%c", file_c);
            }

            if (stop) {
                break;
            }
        }

        if (rows != 0) {
            user_c = getc(stdin);
        }
    }

    int closed = fclose(file);
    if (closed) {
        printf("Error while closing the file");
        return 1;
    }

    return 0;
}
