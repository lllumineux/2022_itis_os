#include <stdio.h>
#include <dirent.h>

struct dirent *d;

void list_directory_content(DIR *directory) {
    while ((d = readdir(directory))) {
        printf("%s\n", d -> d_name);
    }
}

int main(int argc, char *argv[], char *envp[]) {
    DIR *current_dir = opendir(".");
    if (current_dir == NULL) {
        printf("Error while opening current directory");
        return 1;
    }

    DIR *specified_dir = opendir(argv[1]);
    if (specified_dir == NULL) {
        printf("Error while opening the specified directory");
        return 1;
    }

    printf("Current directory:\n");
    list_directory_content(current_dir);
    printf("\n\"%s\" directory:\n", argv[1]);
    list_directory_content(specified_dir);

    int closed_current = closedir(current_dir);
    if (closed_current) {
        printf("Error while closing current directory");
        return 1;
    }

    int closed_specified = closedir(specified_dir);
    if (closed_specified) {
        printf("Error while closing the specified directory");
        return 1;
    }

    return 0;
}
