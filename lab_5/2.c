#include <stdio.h>
#include <stdbool.h>

int main(int argc, char *argv[], char *envp[]) {
    FILE* file = fopen(argv[1], "a+");
    if (!file) {
        printf("Error while opening the file");
        return 1;
    }

    bool ctrl_pressed = false;

    while (1) {
        int c = fgetc(stdin);

        if (c == 6) {
            ctrl_pressed = true;
        } else if (ctrl_pressed && c == 10) {
            break;
        } else {
            ctrl_pressed = false;

            int wrote = fputc(c, file);
            if (!wrote){
                printf("Error while writing to the file");
                return 1;
            }
        }
    }

    int closed = fclose(file);
    if (closed) {
        printf("Error while closing the file");
        return 1;
    }

    return 0;
}
