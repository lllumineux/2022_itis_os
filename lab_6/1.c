#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <time.h>

int main() {
    pid_t pid;
    pid = fork();
    if (pid > 0) {
        pid = fork();
        system("ps -x");
    }

    struct timespec mt;
    clock_gettime (CLOCK_REALTIME, &mt);
    time_t raw_time;
    struct tm *info;
    char buffer[80];
    time(&raw_time);
    info = localtime(&raw_time);
    strftime(buffer, 80, "%H:%M:%S", info);

    printf ("Это процесс, его pid=%d\nА pid его родительского процесса=%d\nА время сейчас - %s.%.3ld\n\n",
            getpid(), getppid(), buffer, mt.tv_nsec / 1000000);
}
